import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import r2_score
from sklearn.cross_validation import train_test_split


class Regression:
    filepath = './training_2008.csv'
    dataframe = []
    dependent_feature = None

    def __init__(self, dependent_feature='DepDelay'):
    # =============== TODO: Your code here ===============
    # One of subtask. Receives dataset, must return prediction vector
    # DepDelay - flight departure delay. You should predict departure delay depending on other features.
    # You should add the code that will:
    #   - read dataset from file
        dataframe = self.load_dataframe()
        self.dependent_feature = dependent_feature
        self.coolMethodThatWillDoAllTheNeededStuffWithDataset(dataframe)
        pass

    def load_dataframe(self):
        return pd.read_csv(self.filepath, ',')

    def clean_dataframe(self, dataframe):
        dataframe = self.remove_empty_columns(dataframe)

        correlated_colums = self.get_correlated_columns(dataframe.corr())
        correlated_df = dataframe[correlated_colums]

        return correlated_df

    def remove_empty_columns(self, dataframe):
        dataframe = dataframe.replace('', np.nan, regex=True)
        dataframe = dataframe.dropna(axis=1, how='all')

        return dataframe

    def get_correlated_columns(self, corr_koef):
        corr_fields = []
        for i in corr_koef:
            for j in corr_koef.index[corr_koef[i] > 0.9]:
                if i != j and j not in corr_fields and i not in corr_fields:
                    corr_fields.append(j)
                    # print("%s->%s: r^2=%f" % (i, j, CorrKoef[i][CorrKoef.index == j].values[0]))

        return corr_fields

    def init_models(self):
        models = []
        models.append(RandomForestClassifier(n_estimators=165, max_depth=4, criterion='entropy'))
        models.append(GradientBoostingClassifier(max_depth=4))
        models.append(KNeighborsClassifier(n_neighbors=20))
        models.append(GaussianNB())

        return models

    def coolMethodThatWillDoAllTheNeededStuffWithDataset(self, dataframe):
        cleaned_dataframe = self.clean_dataframe(dataframe)
        cleaned_dataframe = cleaned_dataframe.dropna(how='any')
        column_headers = cleaned_dataframe.columns.get_values().tolist()
        if(self.dependent_feature not in column_headers):
            print('Dependent feature is absent in correlated dataframe.')
            pass

        trg = cleaned_dataframe[[self.dependent_feature]]
        trn = cleaned_dataframe.drop([self.dependent_feature], 1)

        Xtrn, Xtest, Ytrn, Ytest = train_test_split(trn, trg, test_size=0.4)

        models = self.init_models()
        # создаем временные структуры
        TestModels = pd.DataFrame()
        tmp = {}
        cropped_models = models[1:2]
        # для каждой модели из списка
        for model in cropped_models:
            # получаем имя модели
            m = str(model)
            tmp['Model'] = m[:m.index('(')]
            # обучаем модель
            fit = model.fit(Xtrn, Ytrn.values.ravel())
            # вычисляем коэффициент детерминации
            tmp['R2_Y'] = r2_score(Ytest, model.predict(Xtest))
            tmp['feature_importances'] = [round(elem, 3) for elem in model.feature_importances_]#feature_importances_ not_in_every_model
            # записываем данные и итоговый DataFrame
            TestModels = TestModels.append([tmp])
        # делаем индекс по названию модели
        # TestModels.set_index('Model', inplace=True)#some errors here
        print(TestModels)
        # =============== TODO: Your code here ===============
        # This is the method that will prepare dataset
        # You should add the code that will:
        #   - clean dataset from useless features
        #   - split data set to train and test parts
        #   - implement lable encoding and one hot encoding if needed
        #   - create regression model and fit it
        #   - make prediction values of dependant feature
        #   - calculate r2_score to check prediction accuracy
        #   - save the model
        #   - return r2_score, modified dataset, predicted values vector, saved regression model

        pass

Regression()