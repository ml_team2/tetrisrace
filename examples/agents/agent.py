import gym
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import warnings
# regr
import pickle
from sklearn import linear_model
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
# class
import string
import pickle
from sklearn.feature_extraction.text import CountVectorizer
from nltk.corpus import stopwords
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
from sklearn.feature_extraction.text import TfidfTransformer

# from sklearn import metrics # alternative r2_score calc

warnings.filterwarnings("ignore")

def save_obj(obj, name):
    with open('./' + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open('./' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def text_process(mess):
    """
    Takes in a string of text, then performs the following:
    1. Remove all punctuation and asterisks
    2. Remove all stopwords
    3. Returns a list of the cleaned text
    """
    # Check characters to see if they are in punctuation
    nopunc = [char for char in mess if char not in string.punctuation and char != '*']

    # Join the characters again to form the string.
    nopunc = ''.join(nopunc)

    cleaned = [word.lower() for word in nopunc.split() if word.lower() not in stopwords.words('english')]

    return cleaned

global team_name, folder, env_name
team_name = 'ml_team # 2'
folder = 'tetris_race_qlearning'
env_name = 'TetrisRace-v0'  # do not change this
EPISODES_TO_RUN = 600

class TetrisRaceQLearningAgent:
    def __init__(self, env, learning_rate=0.5, discount_factor=0.5,
                 exploration_rate=0.5, exploration_decay_rate=0.5):
        self.learning_rate = learning_rate
        self.discount_factor = discount_factor
        self.exploration_rate = exploration_rate
        self.exploration_decay_rate = exploration_decay_rate
        self.actions = env.unwrapped.actions
        self._num_actions = len(self.actions)
        self.state = None
        self.action = None
        self.round_counter = 0
        self.inital_utility = EPISODES_TO_RUN % 150
        self.q_table = []

        self.trace = []
        self.verified_traces = set()
        # =============== TODO: Your code here ===============
        #  We'll use tabular Q-Learning in our agent, which means
        #  we need to allocate a Q - Table.Think about what exactly
        #  can be represented as states and how many states should
        #  be at all. Besides this remember about actions, which our
        #  agent will do. Q - table must contain notion about the state,
        #  represented as one single integer value (simplest option) and weights
        #  of each action, which agent can do in current env.

        self.wall_iterator = env.unwrapped.wall_iterator  # passed walls counter


    def choose_action(self, observation):
        # =============== TODO: Your code here ===============
        #  Here agent must choose action on each step, solving exploration-exploitation
        #  trade-off. Remember that in general exploration rate is responsible for
        #  agent behavior in unknown world conditions and main motivation is explore world.
        #  Exploitation rate - choose already known actions and moving through known states.
        #  Think about right proportion that parameters for better solution
        self.check_state_exist(observation)
        exploration_rate = self.exploration_rate - (self.round_counter / EPISODES_TO_RUN) + self.exploration_decay_rate

        # _state = tuple(observation[:2])
        # verified_state = [item for item in self.verified_traces if item[1] == _state]
        # if verified_state:
        #     return verified_state[0][0]

        if (np.random.rand() < exploration_rate):
            return np.random.choice(self.actions)

        return self.get_max_utility(self.get_q_entry_by_state(observation))[0]

    def act(self, current_state, action, reward, next_state):
        # =============== TODO: Your code here ===============
        #  Here agent takes action('moves' somewhere), knowing
        #  the value of Q - table, corresponds current state.
        #  Also in each step agent should note that current
        #  'Q-value' can become 'better' or 'worsen'. So,
        #   an agent can update knowledge about env, updating Q-table.
        #   Remember that agent should choose max of Q-value in  each step
        self.check_state_exist(next_state)

        q_current_entry = self.get_q_entry_by_state(current_state)
        q_next_entry = self.get_q_entry_by_state(next_state)

        if reward == 0:#regular move
            reward = -10
        if reward == -1:#win
            reward = 1000

        q_current_entry[action] += self.learning_rate * (reward
                                                           + (self.discount_factor * self.get_max_utility(q_next_entry)[1])
                                                           - self.get_max_utility(q_current_entry)[1])
        #
        # if reward == 0:# regular move
        #     self.trace.append((action, tuple(current_state[:2])))
        #
        # if reward == -1:# win
        #     self.trace.append((action, tuple(current_state[:2])))
        #     for step in self.trace:
        #         self.verified_traces.add(step)
        #
        if reward < 0:# win or crash
            self.round_counter += 1
        #     self.trace = []
        #     rip = tuple(current_state[1:2])

        return self.q_table

    def get_q_entry_by_state(self, state):
        _state = tuple(state[:2])
        return next((x for x in self.q_table if (x[2] == _state)), None)

    def get_max_utility(self, q_entry):
        #[index, utility]
        if self.are_utilities_pristine(q_entry):
            return [np.random.choice(self.actions), self.inital_utility]

        if q_entry[1] > q_entry[0]:
            return [1, q_entry[1]]
        else:
            return [0, q_entry[0]]

    def are_utilities_pristine(self, q_entry):
        return q_entry[0] == self.inital_utility and q_entry[1] == self.inital_utility

    def check_state_exist(self, state):
        # =============== TODO: Your code here ===============
        #  Here agent can write to Q-table new data vector if current
        #  state is unknown for the moment

        # TEAM_NOTE: state => [distance from left border,
        #                      steps alive since last crash,
        #                      msec since last crash]
        _state = tuple(state[:2])

        # [left_utility, right_utility , state]
        q_entry = [self.inital_utility, self.inital_utility, None]
        existing_q_entry = self.get_q_entry_by_state(state)

        if not(existing_q_entry):
            q_entry[2] = _state
            self.q_table.append(q_entry)

        return existing_q_entry


class EpisodeHistory:
    def __init__(self,env,
                 learn_rate,
                 discount,
                 capacity,
                 plot_episode_count=200,
                 max_timesteps_per_episode=200,
                 goal_avg_episode_length=195,
                 goal_consecutive_episodes=100
                 ):
        self.lengths = np.zeros(capacity, dtype=int)
        self.plot_episode_count = plot_episode_count
        self.max_timesteps_per_episode = max_timesteps_per_episode
        self.goal_avg_episode_length = goal_avg_episode_length
        self.goal_consecutive_episodes = goal_consecutive_episodes

        self.lr = learn_rate
        self.df = discount

        self.lvl_step = env.unwrapped.walls_per_level
        self.lvl_num = env.unwrapped.levels
        self.difficulty = env.unwrapped.level_difficulty
        self.point_plot = None
        self.mean_plot = None
        self.level_plots = []
        self.fig = None
        self.ax = None

    def __getitem__(self, episode_index):
        return self.lengths[episode_index]

    def __setitem__(self, episode_index, episode_length):
        self.lengths[episode_index] = episode_length

    def create_plot(self):
        self.fig, self.ax = plt.subplots(figsize=(14, 7), facecolor='w', edgecolor='k')
        self.fig.canvas.set_window_title("Episode Length History. Team {}".format(team_name))

        self.ax.set_xlim(0, self.plot_episode_count + 5)
        self.ax.set_ylim(0, self.max_timesteps_per_episode + 5)
        self.ax.yaxis.grid(True)
        self.ax.set_title("Episode Length History (lr {}, df {})".format(self.lr, self.df))
        self.ax.set_xlabel("Episode #")
        self.ax.set_ylabel("Length, timesteps")
        self.point_plot, = plt.plot([], [], linewidth=2.0, c="#1d619b")
        self.mean_plot, = plt.plot([], [], linewidth=3.0, c="#df3930")
        for i in range(0, self.lvl_num):
            self.level_plots.append(plt.plot([],[], linewidth =1.0, c="#207232",ls ='--'))

    def update_plot(self, episode_index):
        plot_right_edge = episode_index
        plot_left_edge = max(0, plot_right_edge - self.plot_episode_count)

        # Update point plot.
        x = range(plot_left_edge, plot_right_edge)
        y = self.lengths[plot_left_edge:plot_right_edge]
        self.point_plot.set_xdata(x)
        self.point_plot.set_ydata(y)
        self.ax.set_xlim(plot_left_edge, plot_left_edge + self.plot_episode_count)

        # Update levels plots
        for i in range(1, self.lvl_num+1):
            xl = range(plot_left_edge, plot_right_edge)
            yl = np.zeros(len(xl))
            yl[:] = i * self.lvl_step
            cur_lvl_curve = self.level_plots[i - 1][0]
            cur_lvl_curve.set_xdata(xl)
            cur_lvl_curve.set_ydata(yl)
            self.ax.set_xlim(plot_left_edge, plot_left_edge + self.plot_episode_count)

        # Update rolling mean plot.
        mean_kernel_size = 101
        rolling_mean_data = np.concatenate((np.zeros(mean_kernel_size), self.lengths[plot_left_edge:episode_index]))
        rolling_means = pd.rolling_mean(
            rolling_mean_data,
            window=mean_kernel_size,
            min_periods=0
        )[mean_kernel_size:]
        self.mean_plot.set_xdata(range(plot_left_edge, plot_left_edge + len(rolling_means)))
        self.mean_plot.set_ydata(rolling_means)

        # Repaint the surface.
        plt.draw()
        plt.pause(0.0001)

    def is_goal_reached(self, episode_index):
        ''' DO NOT CHANGE THIS FUNCTION CODE.'''
        # From here agent will receive sygnal about end of learning
        arr = self.lengths[episode_index - self.goal_consecutive_episodes + 1:episode_index + 1]
        avg = np.average(arr)
        if self.difficulty == 'Easy':
            answer = avg >= self.goal_avg_episode_length + 0.5
        elif len(arr)>0:
            density = 2 * np.max(arr) * np.min(arr) / (np.max(arr) + np.min(arr))
            answer = avg >= self.goal_avg_episode_length + 0.5 and density >= avg
        else:
            return False

        return answer


class Controler:
    def __init__(self, parent_mode = True , episodes_num = EPISODES_TO_RUN, global_env = []):
        self.team_name =  team_name
        self.exp_dir = folder + '/' + self.team_name
        random_state = 0
        self.agent_history = []
        self.history_f = True

        self.window = 50

        if parent_mode == False:
            # ====== TODO: your code here======
            # To run env with different parameters you can use another values of named variables, such as:
            #
            # walls_num = x --> number of obstacles (walls). The number must be  x > 6 and x % 3 == 0
            # walls_spread = x --> distance between walls. Too small value leads to no solution situation
            # episodes_to_run = x --> number of agent's tries to learn
            # world_type = 'Fat' or 'Thin' --> make objects more thicker or thinner
            # smooth_car_step = 5 -->  smoothness of car moves (value of step by x)
            # level_difficulty ='Easy' or 'Medium' --> change number of bricks in walls
            # car_spawn = 'Random' or 'Center' --> place, where car starts go
            #
            # EXAMPLE:
            # env.__init__(walls_num = 6, walls_spread = 3, episodes_to_run = episodes_num)
            #
            # Best choice will try any of this different options for better understanding and
            # optimizing the solution.

            env = gym.make(env_name)
            env.__init__(smooth_car_step=2, world_type="Thin", walls_num=12, walls_spread=18, episodes_to_run=EPISODES_TO_RUN, level_difficulty='Easy', car_spawn='Center')
            env.seed(random_state)
            np.random.seed(random_state)
            self.learning_rate = lr = 1  # as system fully determined (at the beginning)
            self.discount_factor = df = .3 # as we wait for long-term reward (probably, df should go up when walls_num is high and vice versa
            self.exploration_rate = exr = .4
            self.exploration_decay_rate = exrd = .001

            self.env = gym.wrappers.Monitor(env, self.exp_dir + '/video', force=True, resume=False,
                                            video_callable=self.video_callable)
            episode_history, end_index = self.run_agent(self, lr, df, exr, exrd, self.env,
                                                        verbose=False) #VERBOSE_MOVIE_AFFECT
        else:
            # Here all data about env will received from main script, so
            # each team will work with equal initial conditions
            # ====== TODO: your code here======
            env = global_env
            env.seed(random_state)
            np.random.seed(random_state)

            self.env = gym.wrappers.Monitor(env, self.exp_dir + '/video', force=True, resume=False,
                                            video_callable=self.video_callable)
            episode_history, end_index = self.run_agent(self, self.learning_rate, self.discount_factor,
                                                        self.exploration_rate, self.exploration_decay_rate,
                                                        self.env, verbose=False)

    def run_agent(self, rate, factor, exploration, exp_decay, env, verbose=False):
        max_episodes_to_run = env.unwrapped.total_episodes
        max_timesteps_per_episode = env.unwrapped.walls_num

        goal_avg_episode_length = env.unwrapped.walls_num
        wall_coef = 6 / env.unwrapped.walls_num
        goal_consecutive_episodes = int(wall_coef * self.window)  # how many times agent can consecutive run succesful

        plot_episode_count = 200
        plot_redraw_frequency = 10

        # =============== TODO: Your code here ===============
        #   Create a Q-Learning agent with proper parameters.
        #   Think about what learning rate and discount factor
        #   would be reasonable in this environment.

        agent = TetrisRaceQLearningAgent(env,
                                         learning_rate=rate,
                                         discount_factor=factor,
                                         exploration_rate=exploration,
                                         exploration_decay_rate=exp_decay
                                         )
        # ====================================================
        episode_history = EpisodeHistory(env,
                                         learn_rate=rate,
                                         discount=factor,
                                         capacity=max_episodes_to_run,
                                         plot_episode_count=plot_episode_count,
                                         max_timesteps_per_episode=max_timesteps_per_episode,
                                         goal_avg_episode_length=goal_avg_episode_length,
                                         goal_consecutive_episodes=goal_consecutive_episodes)
        episode_history.create_plot()

        finish_freq = [0.5, True]  # desired percent finishes in window, flag to run subtask once
        for episode_index in range(0, max_episodes_to_run):
            timestep_index = 0
            observation = env.reset()

            while True:
                action = agent.choose_action(observation)
                observation_, reward, done, info = env.step(action)  # Perform the action and observe the new state.

                if verbose == True:
                    env.render()
                    # self.log_timestep(timestep_index, action, reward, observation)

                if done and timestep_index < max_timesteps_per_episode - 1:
                    reward = -max_episodes_to_run

                QDF = agent.act(observation, action, reward, observation_)
                observation = observation_

                if done:
                    self.done_manager(self, episode_index, [], [], 'D')
                    if self.done_manager(self, episode_index, [], finish_freq, 'S') and finish_freq[1]:
                        finish_freq[1] = False

                    episode_history[episode_index] = timestep_index + 1
                    if verbose or episode_index % plot_redraw_frequency == 0:
                        episode_history.update_plot(episode_index)

                    if episode_history.is_goal_reached(episode_index):
                        print("Goal reached after {} episodes!".format(episode_index + 1))
                        end_index = episode_index + 1
                        foo = Regression(QDF)
                        self.done_manager(self, [], plt, [], 'P')

                        return episode_history, end_index
                    break
                elif env.unwrapped.wall_iterator - timestep_index > 1:
                    timestep_index += 1
        print('q_table:')
        for q in agent.q_table:
            print(q)
        print('verified trace',  agent.verified_traces)
        print("Goal not reached after {} episodes.".format(max_episodes_to_run))
        end_index = max_episodes_to_run
        return episode_history, end_index

    def done_manager(self, episode_ind, plt, top, mode):
        # Call this function to handle episode end event and for storing some
        # result files, pictures etc

        if mode == 'D':  # work with history data
            refresh_each = 100
            self.agent_history.append(self.env.unwrapped.wall_iterator)
            if episode_ind % refresh_each == 0 and self.history_f:
                root = self.exp_dir.split('/')[0]
                base = '/_data'
                path = root + base
                if not os.path.exists(path):
                    os.makedirs(path)
                with open(path + '/' + self.team_name + '.pickle', 'wb') as f:
                    pickle.dump(self.agent_history, f)
        if mode == 'P':  # work woth progress plot
            path = self.exp_dir + '/learn_curve'
            name = '/W ' + str(self.env.unwrapped.walls_num) + \
                   '_LR ' + str(self.learning_rate) + '_DF ' + str(self.discount_factor)
            if not os.path.exists(path):
                os.makedirs(path)
            plt.savefig(path + name + '.png')
        if mode == 'S':  # call subtasks when condition
            if episode_ind > self.window:
                arr = self.agent_history[episode_ind - self.window: episode_ind]
                mx = np.max(arr)
                ind = np.where(arr == mx)[0]
                count = ind.shape[0]
                prc = count / self.window if mx > self.env.unwrapped.walls_per_level * 2  else 0
                x = self.agent_history
                total_finishes = sum(map(lambda x: x > self.env.unwrapped.walls_per_level * 2, x))

                return prc >= top[0] and total_finishes > 100

    def video_callable(episode_id):
        # call agent draw eact N episodes
        return episode_id % 1200 == 0 #MORE MOVIES

    def log_timestep(self, index, action, reward, observation):
        # print parameters in console
        format_string = "   ".join(['Timestep:{}',
                                    'Action:{}',
                                    'Reward:{}',
                                    'Car pos:{}',
                                    'WallY pos:{}'])
        print('Timestep: format string ', format_string.format(index, action, reward,
                                                               observation[0], observation[1]))

    def save_history(self, history, experiment_dir):
        # Save the episode lengths to CSV.
        filename = os.path.join(experiment_dir, "episode_history.csv")
        dataframe = pd.DataFrame(history.lengths, columns=["length"])
        dataframe.to_csv(filename, header=True, index_label="episode")


class Regression:
    output_result = True
    output_detailed_info = False
    corr_koef = 0.45
    filepath = './training_2008.csv'

    dataframe = None
    dependent_feature = None

    def __init__(self, dataset = None, dependent_feature='DepDelay'):
        # =============== TODO: Your code here ===============
        # One of subtask. Receives dataset, must return prediction vector
        # DepDelay - flight departure delay. You should predict departure delay depending on other features.
        # You should add the code that will:
        #   - read dataset from file
        if dataset == None:
            self.dataframe = self.load_dataframe()
        else:
            self.dataframe = dataset
        self.dependent_feature = dependent_feature

        main()
        pass

    def load_dataframe(self):
        return pd.read_csv(self.filepath, ',')

    def clean_dataframe(self, dataframe):
        dataframe = dataframe.drop(dataframe.columns[0], 1) #remove IDs
        dataframe = self.remove_empty_columns(dataframe)
        dataframe = self.perform_label_encoding(dataframe)

        correlated_colums = self.get_correlated_columns(dataframe.corr())
        correlated_df = dataframe[correlated_colums]
        return correlated_df.dropna(how='any')

    def remove_empty_columns(self, dataframe):
        dataframe = dataframe.replace('', np.nan, regex=True)
        dataframe = dataframe.dropna(axis=1, how='all')
        return dataframe

    def perform_label_encoding(self, dataframe):
        # Probably, in this dataset we can just get rid of non-num values without label_encoding
        label_encoder = LabelEncoder()
        for col in dataframe:
            if(isinstance(dataframe[col][0], str)):
                dataframe[col] = label_encoder.fit_transform(dataframe[col].astype(str))
        return dataframe

    def get_correlated_columns(self, corr_koef):
        corr_fields = []
        for i in corr_koef:
            for j in corr_koef.index[corr_koef[i] > self.corr_koef]:
                if i != j and j not in corr_fields and i not in corr_fields:
                    corr_fields.append(j)
                    if self.output_detailed_info:
                        idx = corr_koef.index == j
                        print("[corr]:%s->%s: r^2=%f" % (i, j, corr_koef[i][idx].values[0]))
        return corr_fields

    def perform_linear_regression(self, dataset):
        result = {}
        X = dataset.drop([self.dependent_feature], 1)
        y = dataset[[self.dependent_feature]]
        lm = linear_model.LinearRegression()

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
        model = lm.fit(X_train, y_train)

        if self.output_detailed_info:
            self.print_delimiter('[Train size]')
            print(X_train.shape, y_train.shape)# (len, amount of columns)
            self.print_delimiter('[Test size]')
            print(X_test.shape, y_test.shape)

        result['r2_score'] = model.score(X_test, y_test)  # the percentage of explained variance of the predictions
        result['prediction_vector'] = lm.predict(X_test)
        # result['r2_score'] = metrics.r2_score(y_test, result['prediction_vector']) #alternative calc of r^2
        result['dataset'] = dataset
        result['model'] = model

        return (result, y_test)

    def print_delimiter(self, descriptor=''):
        return print('=======',descriptor, '=======\n')

    def print_result(self, result, test_dataset):
        cut_to = 5
        print('r^2 score:', result['r2_score'])
        print('prediction_vector (%s):\n' % cut_to, result['prediction_vector'][:cut_to])
        print('test_dataset (%s):\n' % cut_to, test_dataset[:cut_to].to_string(index=False))

        plt.scatter(test_dataset, result['prediction_vector'])
        plt.xlabel('Real value')
        plt.ylabel('Prediction')
        plt.show()

        pass

    def main(self):
        cleaned_dataframe = self.clean_dataframe(self.dataframe)
        if self.output_detailed_info:
            self.print_delimiter('[cleaned ds info]')
            cleaned_dataframe.info()

        column_headers = cleaned_dataframe.columns.get_values().tolist()
        if (self.dependent_feature not in column_headers):
            print('[Error]', 'Dependent feature is absent in correlated dataframe.')
            pass

        regression = self.perform_linear_regression(cleaned_dataframe)
        save_obj(regression[0], 'regression_result')
        if self.output_result:
            self.print_result(regression[0], regression[1])

        # =============== TODO: Your code here ===============
        # This is the method that will prepare dataset
        # You should add the code that will:
        #   - clean dataset from useless features
        #   - split data set to train and test parts
        #   - implement lable encoding and one hot encoding if needed
        #   - create regression model and fit it
        #   - make prediction values of dependant feature
        #   - calculate r2_score to check prediction accuracy
        #   - save the model
        #   - return r2_score, modified dataset, predicted values vector, saved regression model

        pass



class Classification:
    filepath = './salaryDataSet.csv'
    dataset = []
    limitDatasetEntries = None
    median_field_name = 'median'
    merged_field_name = 'merged_text'

    def __init__(self):
        self.limitDatasetEntries = 10
        dataset = self.load_dataset()
        for row in dataset:
            print(row)
        pass

    def load_dataset(self):
        raw_dataset = pd.read_csv(self.filepath, encoding="ISO-8859-1")
        raw_dataset = raw_dataset.replace(np.nan, '')

        return raw_dataset

    def merge_text_fields(self, dataset, merged_field_name, *fields_to_merge):
        dataset_with_merged_text = dataset
        merged_text = ''
        for field in fields_to_merge:
            merged_text += dataset_with_merged_text[field] + ' '

        dataset_with_merged_text[merged_field_name] = merged_text
        return dataset_with_merged_text

    def classify_dataset_by_median(self, dataset, classification_field, result_field_name):
        classified_dataset = dataset
        field_median = dataset[classification_field].median()
        classified_dataset[result_field_name] = np.where(dataset[classification_field] < field_median, 0, 1)
        return classified_dataset

    def get_classification_pipeline(self, text, flag, processor=text_process):
        pipeline = Pipeline([
            ('vectorizer', CountVectorizer(processor, ngram_range=(1, 2))),
            ('tfidf_transformer', TfidfTransformer()),
            ('classifier', MultinomialNB())])

        pipeline.fit(text, flag)

        return pipeline

    def fit_classification_model(self, classifier_pipeline, data):
        k_fold = KFold(n_splits=6)
        scores = []
        precisions = []
        recalls = []
        confusion = np.array([[0, 0], [0, 0]])
        for train_indices, test_indices in k_fold.split(data):
            train_text = data.iloc[train_indices][self.merged_field_name].values
            train_y = data.iloc[train_indices][self.median_field_name].values

            test_text = data.iloc[test_indices][self.merged_field_name].values
            test_y = data.iloc[test_indices][self.median_field_name].values

            classifier_pipeline.fit(train_text, train_y)
            predictions = classifier_pipeline.predict(test_text)

            confusion += confusion_matrix(test_y, predictions)

            score = f1_score(test_y, predictions)
            precision = precision_score(test_y, predictions)
            recall = recall_score(test_y, predictions)

            scores.append(score)
            precisions.append(precision)
            recalls.append(recall)

        total_f1_score = sum(scores) / len(scores)
        precision = sum(precisions) / len(precisions)
        recall = sum(recalls) / len(recalls)

        return classifier_pipeline, total_f1_score, precision, recall

    def coolMethodThatWillDoAllTheNeededStuffWithDataset(self):

        raw_dataset = self.load_dataset()
        raw_dataset.reindex(np.random.permutation(raw_dataset.index))

        classified_by_median_dataset = self.classify_dataset_by_median(raw_dataset, 'SalaryNormalized',
                                                                       self.median_field_name)
        data = self.merge_text_fields(classified_by_median_dataset, self.merged_field_name,
                                      'Title', 'FullDescription', 'LocationNormalized',
                                      'ContractType', 'ContractTime', 'Company', 'Category', 'SalaryRaw',
                                      'SourceName', 'LocationRaw'
                                      )
        classifier_pipeline = self.get_classification_pipeline(data[self.merged_field_name].T.values,
                                                               data[self.median_field_name].T.values)

        classifier_pipeline, f1_score, precision, recall = self.fit_classification_model(classifier_pipeline, data)

        save_obj(classifier_pipeline, 'classification_result')

        print("F1 score = %d" % f1_score())
        print("Precision = %d" % precision)
        print("Recall = %d" % recall)

        return classifier_pipeline, f1_score, precision, recall

def main(env, parent_mode = True):
    obj = Controler
    obj.__init__(obj, parent_mode= parent_mode, global_env= env)


if __name__ == "__main__":
    if 'master.py' not in os.listdir('.'):
        main([],parent_mode=False)
    else:
        main(env, parent_mode)