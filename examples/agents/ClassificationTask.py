import numpy as np
import pandas as pd
import string
import pickle
from sklearn.feature_extraction.text import CountVectorizer
from nltk.corpus import stopwords
from nltk import PorterStemmer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
from sklearn.feature_extraction.text import TfidfTransformer


def text_process(mess):
    """
    Takes in a string of text, then performs the following:
    1. Remove all punctuation and asterisks
    2. Remove all stopwords
    3. Returns a list of the cleaned text
    """
    # Check characters to see if they are in punctuation
    nopunc = [char for char in mess if char not in string.punctuation and char != '*']

    # Join the characters again to form the string.
    nopunc = ''.join(nopunc)

    cleaned = [word.lower() for word in nopunc.split() if word.lower() not in stopwords.words('english')]

    return cleaned


class Classification:
    filepath = './salaryDataSet.csv'
    dataset = []
    limitDatasetEntries = None
    median_field_name = 'median'
    merged_field_name = 'merged_text'

    def __init__(self):
        self.limitDatasetEntries = 10
        dataset = self.load_dataset()
        for row in dataset:
            print(row)
        pass

    def load_dataset(self):
        raw_dataset = pd.read_csv(self.filepath, encoding="ISO-8859-1")
        raw_dataset = raw_dataset.replace(np.nan, '')

        return raw_dataset

    def merge_text_fields(self, dataset, merged_field_name, *fields_to_merge):
        dataset_with_merged_text = dataset
        merged_text = ''
        for field in fields_to_merge:
            merged_text += dataset_with_merged_text[field] + ' '

        dataset_with_merged_text[merged_field_name] = merged_text
        return dataset_with_merged_text

    def classify_dataset_by_median(self, dataset, classification_field, result_field_name):
        classified_dataset = dataset
        field_median = dataset[classification_field].median()
        classified_dataset[result_field_name] = np.where(dataset[classification_field] < field_median, 0, 1)
        return classified_dataset

    def get_classification_pipeline(self, text, flag, processor=text_process):
        pipeline = Pipeline([
            ('vectorizer', CountVectorizer(processor, ngram_range=(1, 2))),
            ('tfidf_transformer', TfidfTransformer()),
            ('classifier', MultinomialNB())])

        pipeline.fit(text, flag)

        return pipeline

    def save_obj(self, obj, name):
        with open('./' + name + '.pkl', 'wb') as f:
            pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

    def load_obj(self, name):
        with open('./' + name + '.pkl', 'rb') as f:
            return pickle.load(f)

    def fit_classification_model(self, classifier_pipeline, data):
        k_fold = KFold(n_splits=6)
        scores = []
        precisions = []
        recalls = []
        confusion = np.array([[0, 0], [0, 0]])
        for train_indices, test_indices in k_fold.split(data):
            train_text = data.iloc[train_indices][self.merged_field_name].values
            train_y = data.iloc[train_indices][self.median_field_name].values

            test_text = data.iloc[test_indices][self.merged_field_name].values
            test_y = data.iloc[test_indices][self.median_field_name].values

            classifier_pipeline.fit(train_text, train_y)
            predictions = classifier_pipeline.predict(test_text)

            confusion += confusion_matrix(test_y, predictions)

            score = f1_score(test_y, predictions)
            precision = precision_score(test_y, predictions)
            recall = recall_score(test_y, predictions)

            scores.append(score)
            precisions.append(precision)
            recalls.append(recall)

        total_f1_score = sum(scores) / len(scores)
        precision = sum(precisions) / len(precisions)
        recall = sum(recalls) / len(recalls)

        return classifier_pipeline, total_f1_score, precision, recall

    def coolMethodThatWillDoAllTheNeededStuffWithDataset(self):

        raw_dataset = self.load_dataset()
        raw_dataset.reindex(np.random.permutation(raw_dataset.index))

        classified_by_median_dataset = self.classify_dataset_by_median(raw_dataset, 'SalaryNormalized',
                                                                       self.median_field_name)
        data = self.merge_text_fields(classified_by_median_dataset, self.merged_field_name,
                                      'Title', 'FullDescription', 'LocationNormalized',
                                      'ContractType', 'ContractTime', 'Company', 'Category', 'SalaryRaw',
                                      'SourceName', 'LocationRaw'
                                      )
        classifier_pipeline = self.get_classification_pipeline(data[self.merged_field_name].T.values,
                                                               data[self.median_field_name].T.values)

        classifier_pipeline, f1_score, precision, recall = self.fit_classification_model(classifier_pipeline, data)

        self.save_obj(classifier_pipeline, 'classification_result')

        print("F1 score = %d" % f1_score())
        print("Precision = %d" % precision)
        print("Recall = %d" % recall)

        return classifier_pipeline, f1_score, precision, recall

Classification().coolMethodThatWillDoAllTheNeededStuffWithDataset()
